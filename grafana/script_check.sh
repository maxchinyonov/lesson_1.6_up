#!/bin/bash
# Скрипт для проверки состояния nginx1 и записили логов в /var/log/grafana/logs в "весёлой и понятной" форме

words1="HELLO!!!
ALLO!!!
GETUP!!!
LOOK_AT!!!"

words2="NGINX_1
NGINX_NODE1
NGINX1
NGINX_NODE_1"

words3="DIED!!!
DOWN!!!
UNRUNNING!!!
KILLED!!!"

words4="
REPAIR_IT!!!
FIX_IT!!!
RESTART_IT!!!
CHECK_IT!!!"

words5="
FUCK_YOU!!!
DO_THIS_SHIT!!!
YOU_ARE_NOOB_IN_NGINX!!!
ARE_YOU_RETARDED?!?!?!"

word1=($words1)
word2=($words2)
word3=($words3)
word4=($words4)
word5=($words5)

num_words1=${#word1[*]}
num_words2=${#word2[*]}
num_words3=${#word3[*]}
num_words4=${#word4[*]}
num_words5=${#word5[*]}



#cat /dev/null > /home/grafana/check
nc -zw1 nginx1 2> /home/grafana/check 
if [[ $(cat /home/grafana/check) = *bad* ]]; then
echo "down" > /home/grafana/check
  echo ______________________________________________________________________ >> /var/log/grafana/logs
  date >> /var/log/grafana/logs 	
  echo -n "${word1[$((RANDOM%num_words1))]} " >> /var/log/grafana/logs
  echo -n "${word2[$((RANDOM%num_words2))]} " >> /var/log/grafana/logs
  echo -n "${word3[$((RANDOM%num_words3))]} " >> /var/log/grafana/logs
  echo -n "${word4[$((RANDOM%num_words4))]} " >> /var/log/grafana/logs
  echo "${word5[$((RANDOM%num_words5))]} " >> /var/log/grafana/logs
  echo ______________________________________________________________________ >> /var/log/grafana/logs

else
echo "up" > /home/grafana/check
  echo ______________________________________________________________________ >> /var/log/grafana/logs
  date >> /var/log/grafana/logs	
  echo "nginx1 is working now" >> /var/log/grafana/logs
  echo ______________________________________________________________________ >> /var/log/grafana/logs

fi

